﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ecrireMessageLavalife.aspx.cs" Inherits="PrjWebCsLAVALIFE.ecrireMessageLavalife" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>


    <style>
        .boite{
            width:200px;
            font-weight:bold;
            border-radius:2px;
            border:2px solid;
        }
        table{
            width:500px;
            font-weight:bold;
            margin:auto;
            border-radius:5px;
            border:2px solid;
            background-color:indianred;
            color:brown;
        }
        .message{
            height: 200px;
           width:490px;
            
        }
            body, html {
  height: 100%;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

.bg-img {
  /* The image used */
  background-image: url("../images/ll_login_img.jpg");

  min-height: 800px;

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}
nav{
    color:white;
}
        
    </style>
</head>
<body>
     <nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
        <img src="images/ll_user_menu_ic_profile-x2.png" /><br />
        <asp:Label ID="lblnom" runat="server" Text="Label"></asp:Label>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        &nbsp;   &nbsp;  &nbsp;  &nbsp; &nbsp;   &nbsp;  &nbsp;  &nbsp;
         &nbsp;   &nbsp;  &nbsp;  &nbsp; &nbsp;   &nbsp;  &nbsp;  &nbsp;
         &nbsp;   &nbsp;  &nbsp;  &nbsp; &nbsp;   &nbsp;  &nbsp;  &nbsp;
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="acceuilLavalife.aspx" title="Home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#" title="discussion">Discussion</a>
                </li>
                
            </ul>
        </div>
    </nav>
     <div  class="bg-img">
    <form id="form1" runat="server">
        <br />
         <br />
         <br />
         <br />
         <br />
            <table >

                <tr>
                    <td>
                        Destinataire :
                    </td>
                    <td>
                        <asp:DropDownList width="370px" CssClass="boite" ID="cboDestinataires" runat="server"></asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Titre :
                    </td>
                    <td>
                        <asp:TextBox  width="370px" CssClass="boite" ID="txtTitre" runat="server" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Message
                    </td>
                   
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox CssClass="message" ID="txtMessage" runat="server" TextMode="MultiLine"></asp:TextBox>
                    </td>
                   
                </tr>

                 <tr>
                    <td>
                        <asp:Button ID="btnEnvoyer" runat="server" Text="Envoyer" OnClick="btnEnvoyer_Click" />
                    </td>
                    <td>
                        <asp:Button ID="btnAnnuler" runat="server" Text="Annuler" OnClick="btnAnnuler_Click"  />
                    </td>
                </tr>

            </table>
       
    </form>
          </div>
</body>
</html>
