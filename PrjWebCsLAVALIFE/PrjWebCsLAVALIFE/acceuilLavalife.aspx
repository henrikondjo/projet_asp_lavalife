﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="acceuilLavalife.aspx.cs" Inherits="PrjWebCsLAVALIFE.acceuilLavalife" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
     

     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>






    <style>
        table{
            background-color:white;
            font-weight:bold;
            color:black;
            border:medium solid aliceblue;
            width:88%;
           margin:auto;
           height:50px;
           
        }
        .btn{
            width:150px;
            color:white;
            background-color:red;
            font-weight:bold;
            border-radius:5px;
        }
          body, html {
  height: 100%;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

.bg-img {
  /* The image used */
  background-image: url("../images/ll_login_img.jpg");

  min-height: 800px;

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}
.lab{
    margin-left:125px;
}
nav{
    color:white;
}
hr{
    color:red;
}
.affiche{
    color:forestgreen;
    text-align:center;
}
.msg{
    text-align:center;
    color:forestgreen;
}
    </style>
</head>
<body>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
       <a class="navbar-brand" href="profilLavalife.aspx" title="profil"> <img src="images/ll_user_menu_ic_profile-x2.png" /></a><br />
        <asp:Label ID="lblnom" runat="server" Text="Label"></asp:Label>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        &nbsp;   &nbsp;  &nbsp;  &nbsp; &nbsp;   &nbsp;  &nbsp;  &nbsp;
         &nbsp;   &nbsp;  &nbsp;  &nbsp; &nbsp;   &nbsp;  &nbsp;  &nbsp;
         &nbsp;   &nbsp;  &nbsp;  &nbsp; &nbsp;   &nbsp;  &nbsp;  &nbsp;
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#" title="Home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ecrireMessageLavalife.aspx" title="discussion">Discussion</a>
                </li>
                
            </ul>
        </div>
    </nav>
     <div  class="bg-img">
    <form id="form1" runat="server">
        <br />
        <br />
         <br />
        <hr />
       <h1 style="text-align:center"><asp:Label ID="lblacceuil" CssClass="affiche" runat="server" Text="" Font-Bold="True"></asp:Label></h1> <br />
       <h3 style="text-align:center"> <asp:Label ID="lblmsg" runat="server" CssClass="msg" Text=""></asp:Label></h3>
        <hr />
        <br />
        
         
        <asp:Label ID="Label1" CssClass="lab" runat="server" Text="Rechercher Utilisateur +"></asp:Label>
            <table >
                <tr>
                    <td>SEXE:</td>
                     <td>
                         <asp:DropDownList ID="cbosexe" runat="server"></asp:DropDownList></td>
                     <td>AGE:</td>
                     <td>
                         <asp:DropDownList ID="cbopreage" runat="server"></asp:DropDownList>

                     </td>
                    <td>TO</td>
                    <td>
                        <asp:DropDownList ID="cbodeage" runat="server"></asp:DropDownList>

                    </td>
                    <td>GROUPE:</td>
                    <td>
                        <asp:DropDownList ID="cbogroupe" runat="server"></asp:DropDownList></td>
               
                <td>RAISON:</td>
                <td>
                    <asp:DropDownList ID="cboraison" runat="server" OnSelectedIndexChanged="cboraison_SelectedIndexChanged"></asp:DropDownList></td>
                    
                       <td> <asp:Button ID="btnrech" runat="server" CssClass="btn" Text="SEARCH" OnClick="btnrech_Click" /></td>
                   
                     </tr>
            </table>
        
        <br />
        <br />
        <br />
        <asp:GridView ID="gridmembre" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" />
            <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
            <RowStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <SortedAscendingCellStyle BackColor="#FDF5AC" />
            <SortedAscendingHeaderStyle BackColor="#4D0000" />
            <SortedDescendingCellStyle BackColor="#FCF6C0" />
            <SortedDescendingHeaderStyle BackColor="#820000" />
            </asp:GridView>
         <br />
       <h1 style="text-align:center"> <asp:Label ID="Label2" runat="server" CssClass="msg" Text="VOS MESSAGES"></asp:Label></h1>
     <asp:Table ID="tableMessages" CssClass="tableau" runat="server" GridLines="Both">  </asp:Table><br />
     <asp:Button style="display:block;margin:auto" ID="btnnewMessage" runat="server" Text="Composer un nouveau Message" OnClick="btnnewMessage_Click"  />
    </form>
         </div>
   
</body>
</html>
