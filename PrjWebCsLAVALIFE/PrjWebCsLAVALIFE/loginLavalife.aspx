﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loginLavalife.aspx.cs" Inherits="PrjWebCsLAVALIFE.loginLavalife" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

    <style>
        body, html {
  height: 100%;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}

.bg-img {
  /* The image used */
  background-image: url("../images/ll_login_img.jpg");

  min-height: 800px;

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}

/* Add styles to the form container */
.container {
  position: absolute;
  right: 0;
  margin: 50px;
  max-width: 500px;
  padding: 16px;
  background-color: white;
}

/* Full-width input fields */
.boites {
  width: 100%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}
.boite:focus {
  background-color: #ddd;
  outline: none;
}

/* Set a style for the submit button */
.btn {
  background-color: red;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.btn:hover {
  opacity: 1;
}

              /*.bg-img {*/
  /* The image used */
  /*background-image: url("../images/ll_login_img.jpg");*/
   /*background-position: 60% 60%;*/
  /*min-height: 380px;*/

  /* Center and scale the image nicely */
  /*background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}

body, html {
  height: 100%;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}*/



/* Add styles to the form container */
/*.container {
  position: absolute;
  right: 0;
  margin: 20px;
  max-width: 300px;
  padding: 16px;
  background-color: peru;
}*/

/* Full-width input fields */
/*.boites {
  width: 30%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}
.boiteage {
  width: 20%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}
.boitesemail {
  width: 70%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}
.boite:focus{
  background-color: navajowhite;
  outline: none;
}*/

/* Set a style for the submit button */
/*.btn {
  background-color: red;
  color: white;
  padding: 16px 20px;
  border: none;
  cursor: pointer;
  width: 100%;
  opacity: 0.9;
}

.btn:hover {
  opacity: 1;
}*/


    </style>
</head>
<body>
      
    <div  class="bg-img">
        <img src="../images/ll_logo_march2015.png" style="margin-left: 0px" />
    <form id="form1" runat="server" class="container">

        <h1>LOGIN</h1>
        <asp:Label ID="lblErreur" runat="server" Text=""  Font-Bold="True" ForeColor="Red"></asp:Label><br />
        <asp:Label ID="lblemail" runat="server" Text="Email Adress"></asp:Label>
        <asp:TextBox ID="txtmail" runat="server" CssClass="boites boite"  TextMode="Email"></asp:TextBox>
      <asp:Label ID="lblmotpass" runat="server"  Text="Password"></asp:Label>
        <asp:TextBox ID="txtmot2pass" runat="server" CssClass="boites boite" TextMode="Password"></asp:TextBox>
        <asp:Button ID="btnlog" runat="server" CssClass="btn" Text="Join Now" OnClick="btnlog_Click" /><br />
        <br />
         <br />
         <br />
        <asp:Label ID="Label1" runat="server" Text="Not a Member yet?"></asp:Label>&nbsp;<asp:LinkButton ID="Linksign" runat="server" ForeColor="Black" OnClick="Linksign_Click">Sign Up</asp:LinkButton>


    </form>
          </div>
</body>
</html>
