﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrjWebCsLAVALIFE
{
    public partial class profilLavalife : System.Web.UI.Page
    {
        static SqlConnection mycon;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblnom.Text = Session["name"] + " " + Session["last"] + "," + Session["idage"] + " ans";
            if (IsPostBack == false)
            {
               
                mycon = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=LavalifeDB;Integrated Security=True");

                Remplirtext();
               

            }
        }

        private void Remplirtext()
        {
          
            Int32 valu = Convert.ToInt32(Session["idref"]);
            string sql = "SELECT Nom,Prenom,Email,Motdepasse FROM Membres WHERE RefMembre=" + valu;
            mycon.Open();
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            SqlDataReader myreader = mycmd.ExecuteReader();
            if (myreader.Read() == true)
            {
                txtnom.Text = myreader["Nom"].ToString();
                txtprenom.Text= myreader["Prenom"].ToString();
                txtemail.Text= myreader["Email"].ToString();
                txtmotpass.Text= myreader["Motdepasse"].ToString();
                myreader.Close();
                mycon.Close();
            }
        }

        protected void btnAnnuler_Click(object sender, EventArgs e)
        {
            Response.Redirect("acceuilLavalife.aspx");
        }

        protected void btnmodifier_Click(object sender, EventArgs e)
        {

            Int32 valu = Convert.ToInt32(Session["idref"]);
            string valnom = txtnom.Text.ToString();
            string valprenom = txtprenom.Text.ToString();
            string valemail = txtemail.Text.ToString();
            string valpass = txtmotpass.Text.ToString();
            string sql = "UPDATE Membres SET Nom='" + valnom + "',Prenom='" + valprenom + "',Email='" + valemail + "'," +
                " Motdepasse='" + valpass + "' WHERE RefMembre=" + valu;
            mycon.Open();
            SqlCommand cmd = new SqlCommand(sql, mycon);
            cmd.ExecuteNonQuery();
            mycon.Close();
            Response.Redirect("acceuilLavalife.aspx");
        }
    }
}