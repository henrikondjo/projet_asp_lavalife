﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrjWebCsLAVALIFE
{
    public partial class lireMessageLavalife : System.Web.UI.Page
    {
        static SqlConnection mycon;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblnom.Text = Session["name"] + " " + Session["last"] + "," + Session["idage"] + " ans";
            Int32 Ref = Convert.ToInt32( Request.QueryString["idRefmsg"]);
            mycon = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=LavalifeDB;Integrated Security=True");
            mycon.Open();
            String sql = "SELECT Messages.Titre,Messages.Envoyeur,Membres.Nom,Messages.Message,Messages.DateCreation FROM Messages,Membres WHERE Membres.RefMembre=Messages.Envoyeur AND Messages.RefMessage=" + Ref;
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            SqlDataReader myreader = mycmd.ExecuteReader();
            if (myreader.Read() == true)
            {
                lblTitre.Text = myreader["Titre"].ToString();
                lblDate.Text = myreader["DateCreation"].ToString();
                lblMessage.Text = myreader["Message"].ToString();

            }
            myreader.Close();

            //requete pour mettre a jor le champ nouveau

            sql = "UPDATE Messages SET Nouveau='False' WHERE RefMessage=" + Ref;
            SqlCommand cmd = new SqlCommand(sql, mycon);
            cmd.ExecuteNonQuery();
            mycon.Close();

        }

        protected void btnRetourAcceuil_Click(object sender, EventArgs e)
        {
            Server.Transfer("acceuilLavalife.aspx");
        }
    }
}