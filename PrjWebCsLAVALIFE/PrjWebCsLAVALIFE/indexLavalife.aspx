﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="indexLavalife.aspx.cs" Inherits="PrjWebCsLAVALIFE.indexLavalife" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="../styles/stylelaval.css" />

</head>
<body>
    <form id="form1" runat="server">
        <img src="../images/ll_logo_march2015.png" style="margin-left: 0px" />
                    <div class="container">
            		<div class="col-sm-12 col-md-7 " style="text-align:center">
    			<img class="main-img" src="../images/lavalife_regist_text_header_v1.jpg">
                        <hr />
    			
    			<p>    			
    				<span>Lavalife wants to put the excitement back in dating. We match your interests to help you break the ice and give you online dating tips along the way to make sure you have the best experience possible. Get started today.</span>
    			</p>    			
    			
    			<div class=" col-md-12 ">
    				<div class="col-md-1 col-sm-1 col-xs-0"></div>
    				
    				<div class="col-md-5 col-sm-5 col-xs-6">
    					<asp:Button ID="btnsign" CssClass="button" runat="server" Text="SIGN UP" OnClick="btnsign_Click"  />&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="btnlogin" runat="server" CssClass="buttonlog" Text="LOGIN" OnClick="btnlogin_Click" />
    				</div>
    				
    				</div>
    				</div>			
    			</div>
        
    </form>
</body>
</html>
