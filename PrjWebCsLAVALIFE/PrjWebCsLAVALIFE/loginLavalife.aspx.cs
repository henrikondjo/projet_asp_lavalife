﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrjWebCsLAVALIFE
{
    public partial class loginLavalife : System.Web.UI.Page
    {
        static SqlConnection mycon;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                mycon = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=LavalifeDB;Integrated Security=True");
            }

        }

        protected void Linksign_Click(object sender, EventArgs e)
        {
            Server.Transfer("inscrireLavalife.aspx");
        }

        protected void btnlog_Click(object sender, EventArgs e)
        {
            string mail=txtmail.Text.Trim();
            string pass=txtmot2pass.Text.Trim();
            string sql = "SELECT Age,Nom,Prenom,RefMembre FROM Membres WHERE Email='" + mail + "' AND Motdepasse='" + pass + "'";
            mycon.Open();
            SqlCommand mycmd=new SqlCommand(sql,mycon);
            SqlDataReader myreader=mycmd.ExecuteReader();
            if (myreader.Read() == true)
            {
                Session["idref"] = myreader["RefMembre"];
                Session["name"] = myreader["Nom"];
                Session["last"] = myreader["Prenom"];
                Session["idage"] = myreader["Age"];
                myreader.Close();
                mycon.Close();
                Server.Transfer("acceuilLavalife.aspx");
            }
            else
            {
                myreader.Close();
                mycon.Close();
                lblErreur.Text = "Email ou Mot de passe Invalid.";
            }

        }
    }
}