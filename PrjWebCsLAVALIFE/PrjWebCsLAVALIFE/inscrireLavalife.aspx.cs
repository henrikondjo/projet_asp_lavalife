﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace PrjWebCsLAVALIFE
{
    public partial class inscrireLavalife : System.Web.UI.Page
    {
        static SqlConnection mycon;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack == false)
            {
                mycon = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=LavalifeDB;Integrated Security=True");
            
                Remplirjour();
                Remplirannee();
                Remplirmois();
                Remplirraison();
                Remplirethnique();
                Remplirsexe();
               
            }
        }

        private void Remplirsexe()
        {
            radSexe.Items.Add(new ListItem("Homme"));
            radSexe.Items.Add(new ListItem("Femme"));
        }

        private void Remplirethnique()
        {
            cboethnique.Items.Add(new ListItem("Selectionner"));
            cboethnique.Items.Add(new ListItem("Noir"));
            cboethnique.Items.Add(new ListItem("Europeen"));
            cboethnique.Items.Add(new ListItem("Asiatique"));
        }

        private void Remplirraison()
        {
            cboraison.Items.Add(new ListItem("Selectionner"));
            cboraison.Items.Add(new ListItem("amour"));
            cboraison.Items.Add(new ListItem("amitié"));
            cboraison.Items.Add(new ListItem("autre interêt"));
        }

        private void Remplirmois()
        {
            cbomois.Items.Add(new ListItem("Mois"));
            cbomois.Items.Add(new ListItem("Janvier"));
            cbomois.Items.Add(new ListItem("Fevrier"));
            cbomois.Items.Add(new ListItem("Mars"));
            cbomois.Items.Add(new ListItem("Avril"));
            cbomois.Items.Add(new ListItem("Mai"));
            cbomois.Items.Add(new ListItem("Juin"));
            cbomois.Items.Add(new ListItem("Juillet"));
            cbomois.Items.Add(new ListItem("Août"));
            cbomois.Items.Add(new ListItem("Septembre"));
            cbomois.Items.Add(new ListItem("Octobre"));
            cbomois.Items.Add(new ListItem("Novembre"));
            cbomois.Items.Add(new ListItem("Decembre"));
            
        }

        private void Remplirannee()
        {
            ListItem elm = new ListItem();
            elm.Text = "Annee";
            cboann.Items.Add(elm);
            for (int i = 1913; i < 2024; i++)
            {
                elm = new ListItem();
                elm.Text = i.ToString();
                cboann.Items.Add(elm);
            }
        }

        private void Remplirjour()
        {
            ListItem elm = new ListItem();
            elm.Text = "Jour";
            cbojour.Items.Add(elm);
            for (int i = 1; i < 32; i++)
            {
                elm = new ListItem();
                elm.Text = i.ToString();
                cbojour.Items.Add(elm);
            }
        }

        protected void btninscrire_Click(object sender, EventArgs e)
        {
            string last = txtlastname.Text.Trim();
            string first=txtfirstname.Text.Trim();
            string rais = cboraison.SelectedItem.Text;
            string ehtn = cboethnique.SelectedItem.Text;
            string sex=radSexe.SelectedItem.Text;
            string mail = txtEmail.Text.Trim();
            Int32 ann = DateTime.Now.Year -  Convert.ToInt32(cboann.SelectedItem.Text);  
            string motpass=txtMot2passe.Text.Trim();
            string sql = "INSERT INTO Membres(Nom,Prenom,Age,Email,Motdepasse,Raison,Sexe,Groupe)";
           
           sql += " VALUES('" + last + "','" + first + "'," + ann + ",'" + mail + "','" + motpass + "','" + rais + "','" + sex + "','" + ehtn + "')";
            mycon.Open();
            SqlCommand mycmd = new SqlCommand(sql, mycon);

            mycmd.ExecuteNonQuery();
            mycon.Close();
            Server.Transfer("loginLavalife.aspx");


        }

        protected void Linklogin_Click(object sender, EventArgs e)
        {
            Server.Transfer("loginLavalife.aspx");
        }
    }
}