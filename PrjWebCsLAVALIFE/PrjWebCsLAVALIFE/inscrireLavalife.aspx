﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="inscrireLavalife.aspx.cs" Inherits="PrjWebCsLAVALIFE.inscrireLavalife" %>

<!DOCTYPE html>
<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void cboethnique_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
</script>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style>
        .bg-img {
  /* The image used */
  background-image: url("../images/ll_step1_img.jpg");
   /*background-position: 60% 60%;*/
  min-height: 380px;

  /* Center and scale the image nicely */
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}

body, html {
  height: 100%;
  font-family: Arial, Helvetica, sans-serif;
}

* {
  box-sizing: border-box;
}



/* Add styles to the form container */
.container {
  position: absolute;
  right: 0;
  margin: 20px;
  max-width: 300px;
  padding: 16px;
  background-color: peru;
}

/* Full-width input fields */
.boites {
  width: 30%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}
.boiteage {
  width: 20%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}
.boitesemail {
  width: 50%;
  padding: 15px;
  margin: 5px 0 22px 0;
  border: none;
  background: #f1f1f1;
}
.boite:focus{
  background-color: navajowhite;
  outline: none;
}

/* Set a style for the submit button */
.btn {
  background-color: red;
  color: white;
  padding: 16px 20px;
  border: none;
   left: 150px;
  text-align:center;
  cursor: pointer;
  width: 50%;
  opacity: 0.9;
}

.btn:hover {
  opacity: 1;
}


    </style>
</head>
<body>
     <div class="bg-img" >
    <form id="form1" runat="server" style="margin-left:25px">
       <h1 style="color:saddlebrown">Create your profile</h1>
        <h3 style="color:saddlebrown">AND RECEIVE A 7-DAY FREE TRIAL</h3><br />
        <br />
        <asp:Label ID="lblfirstname" runat="server" Text="First Name"></asp:Label><br />
        <asp:TextBox ID="txtfirstname" cssclass="boite boites" runat="server"></asp:TextBox>&nbsp;
        <asp:RequiredFieldValidator ID="reqnom" runat="server" ErrorMessage="RequiredFieldValidator" ForeColor="#FF3300" ControlToValidate="txtfirstname" Text="*firstname requis!"></asp:RequiredFieldValidator><br />
       <asp:Label ID="lbllastname" runat="server" Text="Last Name"></asp:Label><br />
        <asp:TextBox ID="txtlastname" cssclass="boite boites" runat="server"></asp:TextBox><br />
        <asp:Label ID="lblgroupe" runat="server" Text="Groupe Ehtnique"></asp:Label><br />
        <asp:DropDownList ID="cboethnique" CssClass="boite boites" runat="server" OnSelectedIndexChanged="cboethnique_SelectedIndexChanged"></asp:DropDownList><br />
        <asp:Label ID="lblraison" runat="server" Text="Raison"></asp:Label><br />
        <asp:DropDownList ID="cboraison" CssClass="boite boites" runat="server"></asp:DropDownList><br />
        <asp:Label ID="lblsexe" runat="server" Text="Sexe"></asp:Label>&nbsp;&nbsp;<asp:RadioButtonList ID="radSexe" runat="server"></asp:RadioButtonList><br />
        <asp:Label ID="lblemail" runat="server" Text="Email Address"></asp:Label><br />
        <asp:TextBox ID="txtEmail" runat="server" CssClass="boitesemail"  TextMode="Email"></asp:TextBox><br />
        <asp:Label ID="lblAge" runat="server" Text="Age"></asp:Label><br />&nbsp;&nbsp;
        <asp:DropDownList ID="cbomois" CssClass="boite boiteage" runat="server"></asp:DropDownList>&nbsp;&nbsp;
        <asp:DropDownList ID="cbojour" CssClass="boite boiteage" runat="server"></asp:DropDownList>&nbsp;&nbsp;
         <asp:DropDownList ID="cboann" CssClass="boite boiteage" runat="server"></asp:DropDownList><br />
        <asp:Label ID="lblmotpass" runat="server" Text="Password"></asp:Label><br />
        <asp:TextBox ID="txtMot2passe" runat="server" CssClass="boitesemail"  TextMode="Password"></asp:TextBox><br />
       &nbsp;&nbsp;  &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;<asp:Button ID="btninscrire" runat="server" CssClass="btn btn" Text="Join Now" OnClick="btninscrire_Click" />
    
      
         <br />
        <asp:Label ID="Label1" runat="server" Text="Already a member?"></asp:Label>&nbsp;<asp:LinkButton ID="Linklogin" runat="server" ForeColor="Black" OnClick="Linklogin_Click" >Log In</asp:LinkButton>

    </form>
         </div>
</body>
</html>
