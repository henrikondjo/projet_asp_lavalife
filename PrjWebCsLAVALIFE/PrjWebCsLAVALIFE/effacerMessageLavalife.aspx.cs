﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrjWebCsLAVALIFE
{
    public partial class effacerMessageLavalife : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Int32 Ref = Convert.ToInt32(Request.QueryString["idRefmsg"]);
            SqlConnection mycon = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=LavalifeDB;Integrated Security=True");
            mycon.Open();
            String sql = "DELETE  FROM Messages WHERE RefMessage=" + Ref;
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            mycmd.ExecuteNonQuery();
            //requete pour mettre a jour le champ nouveau
            mycon.Close();
            Server.Transfer("acceuilLavalife.aspx");
        }
    }
}