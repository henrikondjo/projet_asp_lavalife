﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrjWebCsLAVALIFE
{
    public partial class ecrireMessageLavalife : System.Web.UI.Page
    {
        static SqlConnection mycon;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblnom.Text = Session["name"] + " " + Session["last"] + "," + Session["idage"] + " ans";
            if (IsPostBack == false)
            {
                mycon = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=LavalifeDB;Integrated Security=True");

                mycon.Open();
                String sql = "SELECT RefMembre,Nom,Prenom FROM Membres ORDER BY Nom";
                SqlCommand mycmd = new SqlCommand(sql, mycon);
                SqlDataReader myreader = mycmd.ExecuteReader();

                while (myreader.Read() == true)
                {
                    ListItem elm = new ListItem();
                    elm.Text = myreader["Nom"].ToString() + " ( " + myreader["Prenom"].ToString() + " ) ";
                    elm.Value = myreader["RefMembre"].ToString();
                    cboDestinataires.Items.Add(elm);
                }
                myreader.Close();
                mycon.Close();
            }
        }

        protected void btnAnnuler_Click(object sender, EventArgs e)
        {
            Server.Transfer("acceuilLavalife.aspx");
        }

        protected void btnEnvoyer_Click(object sender, EventArgs e)
        {
            //recuperer les valeurs entrees
            String tit = txtTitre.Text.Trim();
            String msg = txtMessage.Text.Trim();
            Int32 refenvoveur = Convert.ToInt32(Session["idref"]);
            Int32 refReceveur = Convert.ToInt32(cboDestinataires.SelectedItem.Value);
            mycon.Open();
            //requete


            String sql = "INSERT INTO Messages (Titre,Message,Envoyeur,Receveur,DateCreation,Nouveau) " +
                "values('" + tit + "','" + msg + "'," + refenvoveur + "," + refReceveur + "," + DateTime.Now.ToShortDateString() + ",'True')";
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            mycmd.ExecuteNonQuery();
            mycon.Close();
            Response.Redirect("acceuilLavalife.aspx");
        }
    }
}