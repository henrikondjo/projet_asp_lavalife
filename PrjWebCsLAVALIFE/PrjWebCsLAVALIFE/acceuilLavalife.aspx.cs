﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PrjWebCsLAVALIFE
{
    public partial class acceuilLavalife : System.Web.UI.Page
    {
        static SqlConnection mycon;
        protected void Page_Load(object sender, EventArgs e)
        {
            lblnom.Text = Session["name"] + " " + Session["last"] + "," + Session["idage"] + " ans";
            if (IsPostBack == false)
            {
               
                mycon = new SqlConnection(@"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=LavalifeDB;Integrated Security=True");
                
                Remplirethnique();
                Remplirraison();
                Remplirsexe();
                Remplirpre();
                Remplirde();
                
            }
            string valsexe = cbosexe.SelectedItem.Text;
            Int32 valpreage = Convert.ToInt32(cbopreage.SelectedItem.Text);
            Int32 valdeage = Convert.ToInt32(cbodeage.SelectedItem.Text);
            string valgroupe = cbogroupe.SelectedItem.Text;
            string valraison = cboraison.SelectedItem.Text;
            String sql = "SELECT Nom,Prenom,Age,Email,Sexe,Groupe FROM Membres WHERE Sexe='" + valsexe + "' AND Groupe='" + valgroupe + "' AND" +
                " Raison='" + valraison + "' AND Age BETWEEN "+valpreage+" AND "+valdeage;
            mycon.Open();
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            SqlDataReader myreader = mycmd.ExecuteReader();
            gridmembre.DataSource = myreader;
            gridmembre.DataBind();
            mycon.Close();
            Int16 nombreMsg = 0;
            mycon.Open();
            Int32 refM = Convert.ToInt32(Session["idref"]);
            //requete de recuperation des messages recu par ce membre
            String sql2 = "SELECT Messages.RefMessage,Messages.Titre,Messages.Nouveau,Messages.Envoyeur,Membres.Nom FROM Messages,Membres WHERE Membres.RefMembre=Messages.Envoyeur AND Messages.Receveur=" + refM;
            SqlCommand mycmd2 = new SqlCommand(sql2, mycon);
            SqlDataReader myreader2 = mycmd2.ExecuteReader();
            //creer la ligne de titre pour le tableau
            TableRow maligne = new TableRow();
            maligne.BackColor = Color.Black;
            maligne.ForeColor = Color.White;
            TableCell macol = new TableCell();
            macol.Text = "Titres";
            macol.Width = 450;
            maligne.Cells.Add(macol);
            macol = new TableCell();
            macol.Text = "Provenance";
            macol.Width = 225;
            maligne.Cells.Add(macol);
            macol = new TableCell();
            macol.Text = "Actions";
            macol.Width = 225;
            maligne.Cells.Add(macol);
            tableMessages.Rows.Add(maligne);
            //boucler a travers les messages recus par ce membre
            while (myreader2.Read() == true)
            {
                //creer une ligne(ou row) dans le tableau pour chaque message
                maligne = new TableRow();
                macol = new TableCell();
                macol.Text = myreader2["Titre"].ToString();
                maligne.Cells.Add(macol);
                macol = new TableCell();
                macol.Text = myreader2["Nom"].ToString();
                maligne.Cells.Add(macol);
                macol = new TableCell();
                Int32 val = Convert.ToInt32(myreader2["RefMessage"].ToString());
                macol.Text = "<a href='lireMessageLavalife.aspx?idRefmsg=" + val + "'>Lire</a>&nbsp;&nbsp;&nbsp; <a href='effacerMessageLavalife.aspx?idRefmsg=" + val + "'>Effacer</a> ";
                maligne.Cells.Add(macol);
                //verifier ci message est nouveau et changer la couleur par consequence
                tableMessages.Rows.Add(maligne);
                if (myreader2["Nouveau"].ToString() == "True")
                {
                    maligne.BackColor = Color.LightYellow;
                }

                nombreMsg++;
            }
            myreader2.Close();
            mycon.Close();
            lblacceuil.Text = "Bonjour," + Session["name"] + " " + Session["last"];
            lblmsg.Text = "Vous avez " + nombreMsg + " nouveaux messages";
        }

        private void Remplirsexe()
        {
            cbosexe.Items.Add(new ListItem("Homme"));
            cbosexe.Items.Add(new ListItem("Femme"));
        }

        private void Remplirethnique()
        {
            
           
            cbogroupe.Items.Add(new ListItem("Noir"));
            cbogroupe.Items.Add(new ListItem("Europeen"));
            cbogroupe.Items.Add(new ListItem("Asiatique"));
        }
        private void Remplirraison()
        {
           
            cboraison.Items.Add(new ListItem("amour"));
            cboraison.Items.Add(new ListItem("amitié"));
            cboraison.Items.Add(new ListItem("autre interêt"));
        }
        private void Remplirpre()
        {
            
            for (int i = 20; i < 50; i++)
            {
                 ListItem elm = new ListItem();
                elm.Text = i.ToString();
                cbopreage.Items.Add(elm);
            }
        }
        private void Remplirde()
        {
            ListItem elm = new ListItem();
            elm.Text = "21";
            for (int i = 22; i < 50; i++)
            {
                 elm = new ListItem();
                elm.Text = i.ToString();
                cbodeage.Items.Add(elm);
            }
        }

        protected void cboraison_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        protected void btnrech_Click(object sender, EventArgs e)
        {
            string valsexe = cbosexe.SelectedItem.Text;
            Int32 valpreage = Convert.ToInt32(cbopreage.SelectedItem.Text);
            Int32 valdeage = Convert.ToInt32(cbodeage.SelectedItem.Text);
            string valgroupe = cbogroupe.SelectedItem.Text;
            string valraison = cboraison.SelectedItem.Text;
            String sql = "SELECT Nom,Prenom,Age,Email,Sexe,Groupe FROM Membres WHERE Sexe='" + valsexe + "' AND Groupe='" + valgroupe + "' AND" +
                " Raison='" + valraison + "' AND Age BETWEEN " + valpreage + " AND " + valdeage;
            mycon.Open();
            SqlCommand mycmd = new SqlCommand(sql, mycon);
            SqlDataReader myreader = mycmd.ExecuteReader();
            gridmembre.DataSource = myreader;
            gridmembre.DataBind();
            mycon.Close();

        }

        protected void btnnewMessage_Click(object sender, EventArgs e)
        {
            Server.Transfer("ecrireMessageLavalife.aspx");
        }
    }
}