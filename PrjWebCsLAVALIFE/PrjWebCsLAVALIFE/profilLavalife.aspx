﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="profilLavalife.aspx.cs" Inherits="PrjWebCsLAVALIFE.profilLavalife" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css">

    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.6.1/dist/jquery.slim.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"></script>

    <style>
         table{
            background-color:white;
            font-weight:bold;
            color:black;
            border:medium solid aliceblue;
             width:400px;
           margin:auto;
           height:50px;
           
        }
          body{
            background-color:firebrick;
            font-weight:bold;
        }
          nav{
             color:white;
        }
    </style>
</head>
<body>
     <nav class="navbar navbar-expand-md bg-dark navbar-dark fixed-top">
       <a class="navbar-brand" href="#" title="profil"> <img src="images/ll_user_menu_ic_profile-x2.png" /></a><br />
        <asp:Label ID="lblnom" runat="server" Text="Label"></asp:Label>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        &nbsp;   &nbsp;  &nbsp;  &nbsp; &nbsp;   &nbsp;  &nbsp;  &nbsp;
         &nbsp;   &nbsp;  &nbsp;  &nbsp; &nbsp;   &nbsp;  &nbsp;  &nbsp;
         &nbsp;   &nbsp;  &nbsp;  &nbsp; &nbsp;   &nbsp;  &nbsp;  &nbsp;
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="acceuilLavalife.aspx" title="Home">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="ecrireMessageLavalife.aspx" title="discussion">Discussion</a>
                </li>
                
            </ul>
        </div>
    </nav>
    <br />
     <br />
     <br />
     <br />
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td><asp:Label ID="Label1" runat="server" Text="Nom :"></asp:Label></td>
                    <td><asp:TextBox ID="txtnom" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label2" runat="server" Text="Prenom :"></asp:Label></td>
                    <td><asp:TextBox ID="txtprenom" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td><asp:Label ID="Label3" runat="server" Text="Adresse Email:"></asp:Label></td>
                    <td><asp:TextBox ID="txtemail" runat="server" TextMode="Email"></asp:TextBox></td>
                </tr>
                <tr>
                   <td> <asp:Label ID="Label4" runat="server" Text="Mot de passe :"></asp:Label></td>
                   <td> <asp:TextBox ID="txtmotpass" runat="server" TextMode="Password"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnmodifier" runat="server" CssClass="bouton" Text="Modifier" OnClick="btnmodifier_Click"   />
                    </td>
                    <td>
                        <asp:Button ID="btnAnnuler" CssClass="bouton" runat="server" Text="Annuler" OnClick="btnAnnuler_Click" />
                    </td>
                    <td></td>

                </tr>
            </table>
        </div>
    </form>
</body>
</html>
